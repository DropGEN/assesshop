jdhhu
@extends('layouts.mainlayout')

@section('content')
    <main role="main" class="col-md-5 ml-sm-auto col-lg-10 px-4">
        <div class="card uper">
            <div class="card-header">
                Add Product
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif
                <form method="post" action="{{route('products.add')}}">
                    <div class="form-group">
                        @csrf
                        <label for="name"> Name:</label>
                        <input type="text" class="form-control" name="name"/>
                    </div>

                    <div class="form-group">
                        <label for="price">Price:</label>
                        <input type="number" class="form-control" name="price"/>
                    </div>
                    <div class="form-group">
                        <label for="quantity">Description:</label>
                        <input type="text" class="form-control" name="description"/>
                    </div>

                    <div class="form-group">
                        <input type="hidden" id="status" name="status" value="1">
                    </div>

                    <div class="form-group">
                        <span><i class="fas fa-cloud-upload-alt mr-2" aria-hidden="true"></i>Choose files</span>
                        <input type="file" id="image" name="image">
                    </div>

                    <button type="submit" class="btn btn-primary pull-right">Add Product</button>
                </form>
            </div>
        </div>

{{--        --}}
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Latest Products</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th></th>
                            <th>Name</th>
                            <th>Sku</th>
                            <th>Price</th>
                            <th>Description</th>
                            <th>view</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td> <a href="{{ url('/viewassets/' . $product->id .'/edit') }}" class="btn btn-xs btn-dark">Edit </a> <td>
                                <td>{{ !empty($product->name) ? $product->name : '' }}</td>
                                <td>{{ !empty($product->sku) ? $product->sku : '' }}</td>
                                <td>{{ !empty($product->price) ?  'R' .number_format($product->price, 2): '' }}</td>
                                <td>{{ !empty($product->description) ? $product->description : '' }}</td>
                                <td><a href="{{ url('/viewbid/' . $product->id .'/view') }}" class="btn btn-xs btn-info">view </a><td>
                                <td><a href="{{ url('/viewbid/' . $product->id .'/delete') }}" class="btn btn-xs btn-danger">Delete </a><td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->

            <!-- /.box-footer -->
        </div>

        {{--        data table--}}
        <br>
        <br>

    </main>
@endsection
