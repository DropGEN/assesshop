jdhhu
@extends('layouts.mainlayout')

@section('content')
    <main role="main" class="col-md-5 ml-sm-auto col-lg-10 px-4">
        <div class="card uper">
            <div class="card-header">
                Add Product
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif
                <form class="form-horizontal" method="POST" action="/viewassets/{{ $product->id }}/update"
                      enctype="multipart/form-data">
                    {{ method_field('PATCH') }}
                    @csrf
                    <div class="form-group">

                        <label for="name"> Name:</label>
                        <input type="text" class="form-control" id="name" name="name"
                               value="{{ !empty($product->name) ? $product->name : '' }}"
                               placeholder=" Name"/>
                    </div>

                    <div class="form-group">
                        <label for="price">Price:</label>
                        <input type="number" class="form-control" id="price" name="price"
                               value="{{ !empty($product->price) ? $product->price : '' }}"
                               placeholder=" price"/>
                    </div>
                    <div class="form-group">
                        <label for="quantity">Description:</label>
                        <input type="text" class="form-control" id="description" name="description"
                               value="{{ !empty($product->description) ? $product->description : '' }}"
                               placeholder=" description"/>
                    </div>

                    <div class="form-group">
                        <input type="hidden" id="status" name="status" value="1">
                    </div>

                    <div class="form-group">
                        <span><i class="fas fa-cloud-upload-alt mr-2" aria-hidden="true"></i>Choose files</span>
                        <input type="file" id="image" name="image">
                    </div>

                    <button type="submit" class="btn btn-primary pull-right">Update Product</button>
                </form>
            </div>
        </div>
        {{--        --}}
    </main>
@endsection
