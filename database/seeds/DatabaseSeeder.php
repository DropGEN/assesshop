<?php
use App\User;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $user = new User;
        $user->name = 'The One';
        $user->email = 'admin@shop.co.za';
        $user->password = Hash::make('shop123');
        $user->type = 1;
        $user->status = 1;
        $user->save();

        $user = new User;
        $user->name = 'Thembiso';
        $user->email = 'thembiso@shop.co.za';
        $user->password = Hash::make('thembi123');
        $user->type = 1;
        $user->status = 1;
        $user->save();

        // And now let's generate a few dozen users for our app:
        for ($i = 0; $i < 12; $i++) {
            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => $faker->name,
                'type' => 1,
                'status' => 1,
            ]);
        }

    }
}
