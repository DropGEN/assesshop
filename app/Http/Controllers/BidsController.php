<?php

namespace App\Http\Controllers;

use App\Bids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BidsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('products')->orderBy('created_at', 'asc')->simplePaginate(10);

        $data['products'] = $products;
        return view('viewbid')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bids  $bids
     * @return \Illuminate\Http\Response
     */
    public function show(Bids $bids)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bids  $bids
     * @return \Illuminate\Http\Response
     */
    public function edit(Bids $bids)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bids  $bids
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bids $bids)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bids  $bids
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bids $bids)
    {
        //
    }
}
