<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products =  Product::orderby('id','asc')->offset(0)->limit(10)->get();

        //  $products = DB::table('products')->orderBy('created_at', 'asc')->limit(9);

//        return $products;

        $data['products'] = $products;
        return view('welcome')->with($data);
    }
    /**
     * view product bids
     *
     * @return \Illuminate\Http\Response
     */
    public function viewbid(Product $product){

        return view('bids');
    }


}
