<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'UserController@index')->name('user.page');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/products', 'ProductController@create')->name('products.add');

Route::get('/products',  'ProductController@index')->name('home');

Route::get('/viewbid/{bid}/view', 'BidsController@index');

Route::get('/viewproduct/{product}/bid', 'UserController@viewbid');

// edit
Route::get('/viewassets/{product}/edit', 'ProductController@show');

//
Route::patch('/viewassets/{product}/update', 'ProductController@edit')->name('products.update');

Route::get('/viewbid/{product}/delete', 'ProductController@destroy');
